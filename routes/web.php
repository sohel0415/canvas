<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Auth\SessionController@login')->name('login');
Route::post('/login', 'Auth\SessionController@postLogin')->name('login');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'DashboardController@index');
    Route::get('logout', 'Auth\SessionController@logout');

    Route::get('change_password', 'Auth\PasswordManagerController@changePassword');
    Route::post('change_password', 'Auth\PasswordManagerController@postChangePassword');

    Route::get('lectures', 'LectureController@index');
    Route::get('lectures/download_class_note/{lecture_id}', 'LectureController@downloadClassNote');
    Route::get('lectures/download_lecture_sheet/{lecture_id}', 'LectureController@downloadLectureSheet');
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::prefix('admin')->group(function () {
            Route::resource('lectures', 'LectureController')->except(['show']);
            Route::resource('users', 'UserController')->except(['show']);
            Route::resource('batches', 'BatchController')->except(['show']);
            Route::resource('subjects', 'SubjectController')->except(['show']);
            Route::resource('configs', 'ConfigController')->except(['show']);

            Route::get('passwords/reset/{user_id}', 'PasswordManagerController@resetPassword');
            Route::post('passwords/reset/{user_id}', 'PasswordManagerController@postResetPassword');
        });
    });
});
