<?php

namespace App\Util;

use Illuminate\Support\Facades\Storage;

trait FileUploadUtil
{
    public function uploadFile($folder, $file, $filename = null)
    {
        $this->makeDirectoryIfNotExist($folder);

        if (!$filename) {
            $filename = $file->getClientOriginalName();
        }

        return $file->storeAs($folder, $filename . rand(100, 999) . '.' . $file->extension());
    }

    private function makeDirectoryIfNotExist($folder)
    {
        if (!Storage::exists($folder)) {
            Storage::makeDirectory($folder, 0775, true, true);
        }
    }
}