<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $table = 'lectures';

    protected $fillable = [
        'subject_id', 'lecture_name', 'topic_name', 'recommended_video', 'class_note', 'lecture_sheet', 'created_by'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }
}
