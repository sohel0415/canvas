<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends Model
{
    use SoftDeletes;

    protected $table = 'configs';

    protected $fillable = [
        'title', 'description', 'link', 'config_type'
    ];
}
