<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    protected $table = 'subjects';

    protected $fillable = [
        'name'
    ];

    public function lectures()
    {
        return $this->hasMany(Lecture::class, 'subject_id', 'id');
    }
}
