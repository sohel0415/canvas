<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'batch_id', 'password', 'status', 'last_session', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id', 'role_id');
    }

    public function batch()
    {
        return $this->belongsTo(Batch::class, 'batch_id', 'id');
    }

    public function isAdmin()
    {
        $roles = $this->roles()->pluck('slug')->toArray();

        if (in_array('admin', $roles)) {
            return true;
        }

        return false;
    }

    public function attachRole($slug)
    {
        $role = Role::where('slug', $slug)->first();

        $this->roles()->attach($role);
    }

    public function detachRole($slug)
    {
        $role = Role::where('slug', $slug)->first();

        $this->roles()->detach($role);
    }
}
