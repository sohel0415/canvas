<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $isAdmin = $user->roles()->where('slug', 'admin')->exists();
        if (!$isAdmin) {
            return redirect('/login');
        }
        return $next($request);
    }
}
