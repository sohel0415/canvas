<?php

namespace App\Http\Controllers;

use App\Model\Lecture;
use App\Model\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LectureController extends Controller
{
    public function index(Request $request)
    {
        $subjectId = $request->get('subject_id');

        if (!empty($subjectId)) {
            $lectures = Lecture::with(['subject'])->where('subject_id', $subjectId)->get();
        } else {
            $lectures = Lecture::all();
        }

        $subjects = Subject::all();

        return view('lectures.index', compact('subjects', 'lectures', 'subjectId'));
    }

    public function downloadClassNote($lectureId)
    {
        $lecture = Lecture::find($lectureId);

        if ($lecture) {
            return Storage::download($lecture->class_note);
        }

        abort(404);
    }

    public function downloadLectureSheet($lectureId)
    {
        $lecture = Lecture::find($lectureId);

        if ($lecture) {
            return Storage::download($lecture->lecture_sheet);
        }

        abort(404);
    }
}
