<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $inputs = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'status' => 'active'
        ];

        if (Auth::attempt($inputs)) {
            User::find(Auth::id())->update(['last_session' => Session::getId(), 'last_login' => Carbon::now()]);
            return redirect()->intended('/');
        }

        return redirect()->back()->withInput()->with('errorMessage', 'Invalid Credentials!!');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }
}
