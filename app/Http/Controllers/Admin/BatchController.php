<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Batch\CreateBatchValidation;
use App\Http\Requests\Admin\Batch\EditBatchValidation;
use App\Model\Batch;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batches = Batch::withCount('users')->get();

        return view('admin.batches.index', compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.batches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBatchValidation $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBatchValidation $request)
    {
        Batch::create(['name' => $request->get('name')]);

        return redirect('admin/batches')->with('successMessage', 'Batch created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $batch = Batch::find($id);

        return view('admin.batches.edit', compact('batch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditBatchValidation $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditBatchValidation $request, $id)
    {
        $batch = Batch::find($id);
        if ($batch) {
            $batch->update(['name' => $request->get('name')]);
        }

        return redirect('admin/batches')->with('successMessage', 'Batch updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $batch = Batch::find($id);
        if ($batch) {
            $batch->delete();
        }

        return redirect('admin/batches')->with('successMessage', 'Batch deleted successfully');
    }
}
