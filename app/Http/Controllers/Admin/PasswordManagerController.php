<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ResetPasswordValidation;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PasswordManagerController extends Controller
{
    public function resetPassword($user_id)
    {
        $user = User::find($user_id);

        return view('admin.reset_password', compact('user'));
    }

    public function postResetPassword(ResetPasswordValidation $request, $user_id)
    {
        $user = User::find($user_id);

        if ($user) {
            $user->update(['password' => Hash::make($request->get('password'))]);
        }

        return redirect('admin/users');
    }
}
