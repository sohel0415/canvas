<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Subject\CreateSubjectValidation;
use App\Http\Requests\Admin\Subject\EditSubjectValidation;
use App\Model\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::withCount('lectures')->get();

        return view('admin.subjects.index', compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subjects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSubjectValidation $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSubjectValidation $request)
    {
        Subject::create(['name' => $request->get('name')]);

        return redirect('admin/subjects')->with('successMessage', 'Subject created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::find($id);

        return view('admin.subjects.edit', compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditSubjectValidation $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditSubjectValidation $request, $id)
    {
        $subject = Subject::find($id);
        if ($subject) {
            $subject->update(['name' => $request->get('name')]);
        }

        return redirect('admin/subjects')->with('successMessage', 'Subject updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::find($id);
        if ($subject) {
            $subject->delete();
        }

        return redirect('admin/subjects')->with('successMessage', 'Subject deleted successfully');
    }
}
