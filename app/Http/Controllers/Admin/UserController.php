<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\CreateUserValidation;
use App\Http\Requests\Admin\User\EditUserValidation;
use App\Model\Batch;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['roles', 'batch'])->get();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $batches = Batch::all();

        return view('admin.users.create', compact('batches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateUserValidation $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserValidation $request)
    {
        $inputs = $request->except(['_token', 'password', 'is_admin']);
        $inputs['password'] = Hash::make($request->get('password'));
        $user = User::create($inputs);

        $user->attachRole('user');
        if ($request->has('is_admin')) {
            $user->attachRole('admin');
        }

        return redirect('admin/users')->with('successMessage', 'User created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with(['roles', 'batch'])->find($id);
        $batches = Batch::all();

        return view('admin.users.edit', compact('user', 'batches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditUserValidation $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserValidation $request, $id)
    {
        $inputs = $request->except(['_token', 'is_admin']);
        $inputs['password'] = Hash::make($request->get('password'));

        $user = User::find($id);

        if ($user) {
            $user->update($inputs);
        }

        if ($request->has('is_admin')) {
            $user->attachRole('admin');
        } else {
            $user->detachRole('admin');
        }

        return redirect('admin/users')->with('successMessage', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
        }

        return redirect('admin/users')->with('successMessage', 'User deleted successfully');
    }
}
