<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Config\CreateConfigValidation;
use App\Http\Requests\Admin\Config\EditConfigValidation;
use App\Model\Config;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configs = Config::orderBy('id', 'desc')->get();

        return view('admin.configs.index', compact('configs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.configs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateConfigValidation $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateConfigValidation $request)
    {
        Config::create($request->all());

        return redirect('admin/configs')->with('successMessage', 'Config created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Config::find($id);

        return view('admin.configs.edit', compact('config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditConfigValidation $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditConfigValidation $request, $id)
    {
        $config = Config::find($id);
        if ($config) {
            $config->update($request->all());
        }

        return redirect('admin/configs')->with('successMessage', 'Config updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $config = Config::find($id);
        if ($config) {
            $config->delete();
        }

        return redirect('admin/configs')->with('successMessage', 'Config deleted successfully');
    }
}
