<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Lecture\CreateLectureValidation;
use App\Http\Requests\Admin\Lecture\UpdateLectureValidation;
use App\Model\Lecture;
use App\Model\Subject;
use App\Util\FileUploadUtil;
use Illuminate\Http\Request;
use function Sodium\compare;

class LectureController extends Controller
{
    use FileUploadUtil;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $subjectId = $request->get('subject_id');

        if (!empty($subjectId)) {
            $lectures = Lecture::with(['subject'])->where('subject_id', $subjectId)->orderBy('id', 'desc')->get();
        } else {
            $lectures = Lecture::orderBy('id', 'desc')->get();
        }

        $subjects = Subject::all();

        return view('admin.lectures.index', compact('subjects', 'lectures', 'subjectId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::all();

        return view('admin.lectures.create', compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLectureValidation $request)
    {
        $inputs = $request->only(['subject_id', 'lecture_name', 'topic_name', 'recommended_video']);
        $folder = $inputs['subject_id'] . '/' . $inputs['lecture_name'] . '/';
        $inputs['lecture_sheet'] = $this->uploadFile($folder, $request->file('lecture_sheet'), 'lecture_sheet');
        $inputs['class_note'] = $this->uploadFile($folder, $request->file('class_note'), 'class_note');
        Lecture::create($inputs);

        return redirect('admin/lectures')->with('successMessage', 'Lecture created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lecture = Lecture::find($id);
        $subjects = Subject::all();

        return view('admin.lectures.edit', compact('lecture', 'subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLectureValidation $request, $id)
    {
        $inputs = $request->only(['subject_id', 'lecture_name', 'topic_name', 'recommended_video']);
        $folder = $inputs['subject_id'] . '/' . $inputs['lecture_name'] . '/';
        $inputs['lecture_sheet'] = $this->uploadFile($folder, $request->file('lecture_sheet'), 'lecture_sheet');
        $inputs['class_note'] = $this->uploadFile($folder, $request->file('class_note'), 'class_note');

        $lecture = Lecture::find($id);
        if ($lecture) {
            $lecture->update($inputs);
        }

        return redirect('admin/lectures')->with('successMessage', 'Lecture updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lecture = Lecture::find($id);
        if ($lecture) {
            $lecture->delete();
        }

        return redirect('admin/lectures')->with('successMessage', 'Lecture deleted successfully');
    }
}
