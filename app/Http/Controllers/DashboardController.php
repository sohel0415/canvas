<?php

namespace App\Http\Controllers;

use App\Model\Config;

class DashboardController extends Controller
{
    public function index()
    {
        $liveClassLink = Config::where('config_type', 'live_class_link')->orderBy('id', 'desc')->first();
        $examLink = Config::where('config_type', 'exam_link')->orderBy('id', 'desc')->first();

        return view('dashboard', compact('liveClassLink', 'examLink'));
    }
}
