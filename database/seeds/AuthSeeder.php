<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roleUser = new \App\Model\Role();
        $roleUser->slug = 'user';
        $roleUser->name = 'User';
        $roleUser->is_active = 1;
        $roleUser->save();

        $roleAdmin = new \App\Model\Role();
        $roleAdmin->slug = 'admin';
        $roleAdmin->name = 'Admin';
        $roleAdmin->is_active = 1;
        $roleAdmin->save();

        $this->command->info('Role model seeded!');

        DB::table('users')->delete();

        $user = new \App\Model\User();
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->phone = '01751024502';
        $user->batch_id = null;
        $user->status = 'active';
        $user->password = Hash::make('admin');
        $user->save();

        $role = \App\Model\Role::where('slug', 'admin')->first();
        $role->users()->attach($user);

        $this->command->info('User model seeded!');
    }
}
