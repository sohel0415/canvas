<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('email');
                $table->string('phone');
                $table->integer('batch_id')->unsigned()->nullable();
                $table->string('password');
                $table->enum('status', ['active', 'pending', 'block']);
                $table->string('last_session')->nullable();
                $table->timestamp('last_login')->nullable();
                $table->softDeletes();
                $table->rememberToken();
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->unique('email');
                $table->unique('phone');

                $table->foreign('batch_id')->references('id')->on('batches')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('roles')) {
            Schema::create('roles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('slug');
                $table->string('name');
                $table->boolean('is_active')->default(0);
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->unique('slug');
            });
        }
        if (!Schema::hasTable('role_users')) {
            Schema::create('role_users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id')->unsigned();
                $table->integer('role_id')->unsigned();
                $table->nullableTimestamps();

                $table->engine = 'InnoDB';
                $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_users');
        Schema::drop('roles');
        Schema::drop('users');
    }
}
