<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('lectures')) {
            Schema::create('lectures', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('subject_id')->unsigned();
                $table->string('lecture_name');
                $table->string('topic_name');
                $table->text('recommended_video');
                $table->text('class_note');
                $table->text('lecture_sheet');
                $table->bigInteger('created_by')->unsigned()->nullable();
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectures');
    }
}
