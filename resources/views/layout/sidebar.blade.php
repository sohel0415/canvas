<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
        <img src="{{asset('image/logo.png')}}" alt="logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Canvas</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url('/')}}"
                       class="nav-link {{ (request()->is('/')) ? 'active':''  }}">
                        <i class="nav-icon far fa-file"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('lectures')}}"
                       class="nav-link {{ (request()->is('lectures')) ? 'active':''  }}">
                        <i class="nav-icon far fa-file"></i>
                        <p>
                            Lectures
                        </p>
                    </a>
                </li>
                @if(Auth::user()->isAdmin())
                    <li class="nav-item has-treeview {{ (request()->is('admin/*')) ? 'menu-open':''  }}">
                        <a href="#" class="nav-link {{ (request()->is('admin/*')) ? 'active':''  }}">
                            <i class="nav-icon fas fa-toolbox"></i>
                            <p>
                                Admin Tools
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('admin/lectures')}}"
                                   class="nav-link {{ (request()->is('admin/lectures*')) ? 'active':''  }}">
                                    <i class="nav-icon far fa-file"></i>
                                    <p>
                                        Lectures
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/configs')}}"
                                   class="nav-link {{ (request()->is('admin/configs*')) ? 'active':''  }}">
                                    <i class="nav-icon fas fa-laptop-code"></i>
                                    <p>
                                        Configs
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/users')}}"
                                   class="nav-link {{ (request()->is('admin/users*')) ? 'active':''  }}">
                                    <i class="nav-icon far fa-user"></i>
                                    <p>
                                        Users
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/batches')}}"
                                   class="nav-link {{ (request()->is('admin/batches*')) ? 'active':''  }}">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        Batches
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/subjects')}}"
                                   class="nav-link {{ (request()->is('admin/subjects*')) ? 'active':''  }}">
                                    <i class="nav-icon fas fa-book-open"></i>
                                    <p>
                                        Subjects
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
