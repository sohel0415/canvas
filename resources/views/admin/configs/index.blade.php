@extends('layout.layout')
@section('title', 'Config list')
@section('header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Config list</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Config list</h1>
                </div>
                <div class="col-sm-6">
                    <a href="{{url('admin/configs/create')}}" class="btn btn-primary float-right">
                        <i class="fas fa-plus"></i> Create new</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-body">
                @include('layout.flash_message')
                <table id="example" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="20%">Title</th>
                        <th width="30%">Description</th>
                        <th width="30%">Link</th>
                        <th width="10%">Config type</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($configs as $config)
                        <tr>
                            <td>{{$config->title}}</td>
                            <td>{{$config->description}}</td>
                            <td>{{$config->link}}</td>
                            <td>{{$config->config_type}}</td>
                            <td class="project-actions text-right">
                                <form action="{{url('admin/configs/'.$config->id)}}" method="post"
                                      onsubmit="return confirm('Do you really want to delete?');">
                                    @csrf
                                    @method('delete')
                                    <a class=" btn btn-info btn-sm
                                " href="{{url('admin/configs/'.$config->id.'/edit')}}">
                                        <i class="fas fa-pencil-alt" title="Edit"></i>
                                    </a>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash" title="Delete"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
    <!-- DataTables -->
    <script src="{{asset('resources/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('resources/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('resources/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('resources/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
            $("#example").DataTable({
                "responsive": true,
                "autoWidth": false,
                "order": []
            });
        });
    </script>
@endsection