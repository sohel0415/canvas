@extends('layout.layout')
@section('title', 'Config create')
@section('header')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Config create</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Config create</h1>
                </div>
                <div class="col-sm-6">
                    <a href="{{url('admin/configs')}}" class="btn btn-primary float-right">
                        <i class="fas fa-arrow-left"></i> Back to list</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('admin/configs')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title <span class="color-red">*</span></label>
                                <input type="text" id="title" name="title" value="{{old('title')}}"
                                       class="form-control <?php if ($errors->first('title') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('title') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('title')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="description">Description <span class="color-red">*</span></label>
                                <textarea type="text" id="description" name="description"
                                          class="form-control <?php if ($errors->first('description') != null) echo 'is-invalid'; ?>">{{old('description')}}</textarea>
                                <span class="error <?php if ($errors->first('description') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('description')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="link">Link <span class="color-red">*</span></label>
                                <textarea type="text" id="link" name="link"
                                          class="form-control <?php if ($errors->first('link') != null) echo 'is-invalid'; ?>">{{old('link')}}</textarea>
                                <span class="error <?php if ($errors->first('link') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('link')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="configType">Config type <span class="color-red">*</span></label>
                                <select class="form-control custom-select <?php if ($errors->first('config_type') != null) echo 'is-invalid'; ?>"
                                        name="config_type" id="configType">
                                    <option value="live_class_link"
                                            @if(old('config_type')=='live_class_link') selected @endif>Live class link
                                    </option>
                                    <option value="exam_link"
                                            @if(old('config_type')=='exam_link') selected @endif>Exam link
                                    </option>
                                </select>
                                <span class="error <?php if ($errors->first('config_type') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('config_type')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Create" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
@endsection