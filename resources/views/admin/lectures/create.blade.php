@extends('layout.layout')
@section('title', 'Lecture create')
@section('header')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Lecture create</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Lecture create</h1>
                </div>
                <div class="col-sm-6">
                    <a href="{{url('admin/lectures')}}" class="btn btn-primary float-right">
                        <i class="fas fa-arrow-left"></i> Back to list</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('admin/lectures')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="subjectId">Subject <span class="color-red">*</span></label>
                                <select class="form-control custom-select <?php if ($errors->first('subject_id') != null) echo 'is-invalid'; ?>"
                                        name="subject_id" id="subjectId">
                                    @foreach($subjects as $subject)
                                        <option value="{{$subject->id}}"
                                                @if(old('subject_id')==$subject->id) selected @endif>{{$subject->name}}</option>
                                    @endforeach
                                </select>
                                <span class="error <?php if ($errors->first('subject_id') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('subject_id')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="lectureName">Lecture name <span class="color-red">*</span></label>
                                <input type="text" id="lectureName" name="lecture_name" value="{{old('lecture_name')}}"
                                       class="form-control <?php if ($errors->first('lecture_name') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('lecture_name') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('lecture_name')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="topicName">Topic name <span class="color-red">*</span></label>
                                <input type="text" id="topicName" name="topic_name" value="{{old('topic_name')}}"
                                       class="form-control <?php if ($errors->first('topic_name') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('topic_name') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('topic_name')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recommendedVideo">Recommended video <span class="color-red">*</span></label>
                                <textarea type="text" id="recommendedVideo" name="recommended_video"
                                          class="form-control <?php if ($errors->first('recommended_video') != null) echo 'is-invalid'; ?>">{{old('recommended_video')}}</textarea>
                                <span class="error <?php if ($errors->first('recommended_video') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('recommended_video')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="classNote">Class note <span class="color-red">*</span></label>
                                <input type="file" id="classNote" name="class_note"
                                       class="form-control <?php if ($errors->first('class_note') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('class_note') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('class_note')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="lectureSheet">Lecture sheet <span class="color-red">*</span></label>
                                <input type="file" id="lectureSheet" name="lecture_sheet"
                                       class="form-control <?php if ($errors->first('lecture_sheet') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('lecture_sheet') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('lecture_sheet')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Create" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
@endsection