@extends('layout.layout')
@section('title', 'Lecture list')
@section('header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Lecture list</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Lecture list</h1>
                </div>
                <div class="col-sm-6">
                    <a href="{{url('admin/lectures/create')}}" class="btn btn-primary float-right">
                        <i class="fas fa-plus"></i> Create new</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-group">
                        <label for="subjectId">Subject : </label>
                        <select class="form-control custom-select col-md-6" name="subject_id" id="subjectId"
                                onchange="this.form.submit()">
                            <option selected disabled>Select one</option>
                            @foreach($subjects as $subject)
                                <option value="{{$subject->id}}"
                                        @if($subjectId == $subject->id) selected @endif>{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
                @include('layout.flash_message')
                <table id="example" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Lecture</th>
                        <th>Topic name</th>
                        <th>Recommended video</th>
                        <th>Class note</th>
                        <th>Lecture sheet</th>
                        <th>Subject</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lectures as $lecture)
                        <tr>
                            <td>{{$lecture->lecture_name}}</td>
                            <td>{{$lecture->topic_name}}</td>
                            <td><a href="{{$lecture->recommended_video}}" target="_blank">Go to link</a></td>
                            <td>
                                <a href="{{url('lectures/download_class_note/'.$lecture->id)}}"
                                   target="_blank">Download</a>
                            </td>
                            <td>
                                <a href="{{url('lectures/download_lecture_sheet/'.$lecture->id)}}" target="_blank">Download</a>
                            </td>
                            <td>{{optional($lecture->subject)->name}}</td>
                            <td class="project-actions text-right">
                                <form action="{{url('admin/lectures/'.$lecture->id)}}" method="post"
                                      onsubmit="return confirm('Do you really want to delete?');">
                                    @csrf
                                    @method('delete')
                                    <a class="btn btn-info btn-sm"
                                       href="{{url('admin/lectures/'.$lecture->id.'/edit')}}">
                                        <i class="fas fa-pencil-alt" title="Edit"></i>
                                    </a>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash" title="Delete"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
    <!-- DataTables -->
    <script src="{{asset('resources/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('resources/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('resources/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('resources/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
            $("#example").DataTable({
                "responsive": true,
                "autoWidth": true,
                "order": []
            });
        });
    </script>
@endsection