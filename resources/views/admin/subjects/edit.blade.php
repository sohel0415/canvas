@extends('layout.layout')
@section('title', 'Subject edit')
@section('header')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Subject edit</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Subject edit</h1>
                </div>
                <div class="col-sm-6">
                    <a href="{{url('admin/subjects')}}" class="btn btn-primary float-right">
                        <i class="fas fa-arrow-left"></i> Back to list</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('admin/subjects/'.$subject->id)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="name">Name <span class="color-red">*</span></label>
                                <input type="text" id="name" name="name"
                                       class="form-control <?php if ($errors->first('name') != null) echo 'is-invalid'; ?>"
                                       value="{{old('name',$subject->name)}}">
                                <span class="error <?php if ($errors->first('name') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('name')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
@endsection