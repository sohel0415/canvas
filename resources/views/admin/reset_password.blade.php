@extends('layout.layout')
@section('title', 'Reset password')
@section('header')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Reset password</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Reset password</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('admin/passwords/reset/'.$user->id)}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password"
                                       class="form-control <?php if ($errors->first('password') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('password') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('password')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Reset" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
@endsection