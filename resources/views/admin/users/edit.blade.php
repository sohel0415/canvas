@extends('layout.layout')
@section('title', 'User edit')
@section('header')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">User edit</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>User edit</h1>
                </div>
                <div class="col-sm-6">
                    <a href="{{url('admin/users')}}" class="btn btn-primary float-right">
                        <i class="fas fa-arrow-left"></i> Back to list</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('admin/users/'.$user->id)}}" method="post">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="name">Name <span style="color: red">*</span></label>
                                <input type="text" id="name" name="name" value="{{old('name', $user->name)}}"
                                       class="form-control <?php if ($errors->first('name') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('name') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('name')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="email">Email <span style="color: red">*</span></label>
                                <input type="text" id="email" name="email" value="{{old('email', $user->email)}}"
                                       class="form-control <?php if ($errors->first('email') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('email') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('email')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone <span style="color: red">*</span></label>
                                <input type="text" id="phone" name="phone" value="{{old('phone', $user->phone)}}"
                                       class="form-control <?php if ($errors->first('phone') != null) echo 'is-invalid'; ?>">
                                <span class="error <?php if ($errors->first('phone') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('phone')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="batch">Batch <span style="color: red">*</span></label>
                                <select class="form-control custom-select <?php if ($errors->first('batch_id') != null) echo 'is-invalid'; ?>"
                                        name="batch_id">
                                    <option selected disabled>Select one</option>
                                    @foreach($batches as $batch)
                                        <option value="{{$batch->id}}"
                                                @if(old('batch_id', $user->batch_id)==$batch->id) selected @endif>{{$batch->name}}</option>
                                    @endforeach
                                </select>
                                <span class="error <?php if ($errors->first('batch_id') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('batch_id')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Status <span style="color: red">*</span></label>
                                <select class="form-control custom-select <?php if ($errors->first('status') != null) echo 'is-invalid'; ?>"
                                        name="status">
                                    <option value="active" @if(old('status', $user->status)=='active') selected @endif>
                                        Active
                                    </option>
                                    <option value="pending"
                                            @if(old('status', $user->status)=='pending') selected @endif>Pending
                                    </option>
                                    <option value="block" @if(old('status', $user->status)=='block') selected @endif>
                                        Block
                                    </option>
                                </select>
                                <span class="error <?php if ($errors->first('status') != null) echo 'invalid-feedback'; ?>"
                                      style="display: inline;">{{$errors->first('status')}}</span>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="isAdmin" name="is_admin"
                                       @if(old('_token') !== null) @if(old('is_admin')!=null) checked
                                       @endif @else @if(in_array('admin', $user->roles()->pluck('slug')->toArray())) checked @endif @endif>
                                <label class="form-check-label" for="isAdmin">Allow as admin user</label>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@stop
@section('footer')
@endsection