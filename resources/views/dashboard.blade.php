@extends('layout.layout')
@section('title', 'Dashboard')
@section('header')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h6>Live class link info</h6>
                            <h3>{{optional($liveClassLink)->title}}</h3>

                            <p>{{optional($liveClassLink)->description}}</p>
                        </div>
                        <a href="{{optional($liveClassLink)->link}}" target="_blank" class="small-box-footer">Go to
                            link</a>
                    </div>
                </div>
                <div class="col-lg-6 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h6>Exam link info</h6>
                            <h3>{{optional($examLink)->title}}</h3>

                            <p>{{optional($examLink)->description}}</p>
                        </div>
                        <a href="{{optional($examLink)->link}}" target="_blank" class="small-box-footer">Go to
                            link</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
@section('footer')
@endsection